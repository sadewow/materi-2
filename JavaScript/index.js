// Variables var, let, const
var a = 10
a = 20

let b = "Abi"
b = "Joan"

const c = 25; // cannot change

// Data types
const name = 'John'; // string
const age = 20; // numbers
const rating = 4.5; // numbers
let isCool = true; // boolean
let x = null; // null
let y = undefined; // undefined
let z; // undefined

// Concatenation
console.log('My name is ' + name + ' and I am ' + age);
// Template String
const hello = `My name is ${name} and I am ${age}`;
console.log(hello);
console.log(hello.toUpperCase());
console.log(hello.toLocaleLowerCase());
console.log(hello.substring(0, 15).toUpperCase());
console.log(hello.split(' '));

// Arrays - variables that hold multiple values
const numbers = new Array(1,2,3,4,5); // create array using constructor
const fruits = ['apple', 'orange', 'banana']; // array literals
fruits.push('mango'); // add value to the end
fruits.unshift('strawberry'); // add value to the beginning
fruits.pop(); // remove the last value in an array
console.log(fruits.indexOf('banana')); // check the index of value in an array
console.log(Array.isArray(fruits)); // check if variable is an array

// Objects - variables that defined with key and values
const person = {
    firstName: 'John',
    lastName: 'Doe',
    age: 30,
    hobbies: ['swimming', 'reading', 'watching movies'],
    address: {
        street: '50 main st',
        city: 'Boston',
        state: 'MA'
    }
} // object literals
person.email = 'john@mail.com'; // add a new key and value to the object
console.log(person);

const { firstName, lastName, address: { city } } = person; // object destructuring
console.log(firstName, lastName, 'from', city);

// array of object
const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentitst appointment',
        isCompleted: false
    }
];

const todoJSON = JSON.stringify(todos); // parse object to JSON format

// For
for (let i = 0; i <= todos.length - 1; i++) {
    console.log(todos[i].text)
}

// for of
for (let todo of todos) {
    console.log(todo.text)
}

// While
let i = 0;
while (i < 10) {
    console.log(`While loop number: ${i}`)
    i++
}

// High functional array functions
// forEach, same with for of
todos.forEach((todo) => {
    console.log(todo.text);
});

// map, crete new array
const todoText = todos.map((todo) => {
    return todo.text;
});

// filter, create new array based on criteria
const todoCompleted = todos.filter((todo) => {
    return todo.isCompleted === true;
}).map((todo) => todo.text)

// Conditional
// if
const d = 4;
const e = 20;

if (e === 10) {
    console.log('number is 10')
} else if (d > 10) {
    console.log('number is greater than 10')
} else {
    console.log('number is less than 10')
};

// ternary
const color = e > 10 ? 'green' : 'red';
console.log(color);

// switch-case
switch (color) {
    case 'green':
        console.log('color is green');
        break;
    case 'red':
        console.log('color is red');
        break;
    default:
        console.log('color is NOT green or red');
        break;
}

// function
function addNums(num1, num2) {
    return num1 + num2;
};
console.log(addNums(5,10));

// // constructor function
// function Person(firstName, lastName, dob) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob = new Date(dob);
// }

// // Prototype, cannot use arrow function
// Person.prototype.getBirthYear = function() {
//     return this.dob.getFullYear();
// }

// Person.prototype.getFullName = function() {
//     return `${this.firstName} ${this.lastName}`
// }

class Person {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }

    getBirthYear() {
        return this.dob.getFullYear();
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`
    }
}

// Instantiate object
const person1 = new Person('John', 'Doe', '4-3-1980');
const person2 = new Person('Mary', 'Holland', '4-3-1980');

console.log(person1, person2.getBirthYear(), person2.getFullName());

// -------------- DOM -----------------
// Single element
console.log(document.getElementById('my-form'));
console.log(document.querySelector('h1'))

// Multiple element
const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item))

const ul = document.querySelector('.items');
// ul.lastElementChild.remove() // to remove the last element
// ul.firstElementChild.textContent = 'Hello' // change first element text
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>';
// ul.remove() // to remove the ul element

// const btn = document.querySelector('.btn');
// btn.style.background = 'red';

// btn.addEventListener('click', (e) => {
//     e.preventDefault();
//     console.log(e.target.className)
//     document.querySelector('#my-form').style.background = '#ccc'; // change background
//     document.querySelector('body').classList.add('bg-dark'); // add class
//     document.querySelector('.items').lastElementChild.innerHTML = '<h1>Hello</h1>';
// });

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

const onSubmit = (e) => {
    e.preventDefault();

    if (nameInput.value === '' || emailInput.value === '') {
        msg.classList.add('error');
        msg.innerHTML = 'Please enter all the fields';

        setTimeout(() => msg.remove(), 2000);
    } else {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(`${nameInput.value} : ${emailInput.value}`));
        userList.appendChild(li);

        // clear fields
        nameInput.value = '';
        emailInput.value = '';
    }
}

myForm.addEventListener('submit', onSubmit);